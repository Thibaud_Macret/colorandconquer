extends ColorRect

var posx:int
var posy:int

func _ready():
	posx = get_parent().get_position_in_parent()
	posy = get_position_in_parent()


func _on_Border_gui_input(event):
	if(event is InputEventMouseButton && event.pressed && event.button_index == 1):
		get_parent().get_parent().get_parent().on_tile_click(posx, posy)
