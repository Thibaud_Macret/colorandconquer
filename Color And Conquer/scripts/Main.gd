extends Node2D

var p1Color:Color = Color("3cd50c")
var p2Color:Color = Color("ed3403")

#9 is for wall, 0 is for empty, 1 if for p1, 2 is for p2
var pointsMatrix:Array = [
[1,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,2],]

var playingId:int = 0



func _ready():
	randomize()
	for index in pointsMatrix.size():
		for index2 in pointsMatrix[0].size():
			match pointsMatrix[index][index2]:
				1:
					$Tiles.get_child(index).get_child(index2).color = p1Color
				2:
					$Tiles.get_child(index).get_child(index2).color = p2Color
				9:
					$Tiles.get_child(index).get_child(index2).visible = false



# warning-ignore:unused_argument
func _physics_process(delta):
	if(playingId == 1):
		playerLoop()
	else:
		aiLoop()



func playerLoop():
	pass

func on_tile_click(posx:int, posy:int):
	if(playingId == 1):
		if(test_adjacency(posx, posy, 1) && pointsMatrix[posx][posy]==0):
			pointsMatrix[posx][posy] = 1
			$Tiles.get_child(posx).get_child(posy).color = p1Color
			playingId = 2
			actualise()

func test_adjacency(posx:int, posy:int, target:int)->int:
	var nb_adjacent:int = 0
	if(posx<pointsMatrix.size()-1):
		if(pointsMatrix[posx+1][posy] == target):
			nb_adjacent += 1
	if(posy<pointsMatrix[0].size()-1):
		if(pointsMatrix[posx][posy+1] == target):
			nb_adjacent += 1
	if(posx>0):
		if(pointsMatrix[posx-1][posy] == target):
			nb_adjacent += 1
	if(posy>0):
		if(pointsMatrix[posx][posy-1] == target):
			nb_adjacent += 1
	return nb_adjacent



func aiLoop():
	pick_and_play(determine_possibilities())

func determine_possibilities()->Array:
	var possibilities:Array = []
	var bestScore:int = 0
	for index in pointsMatrix.size():
		for index2 in pointsMatrix[0].size():
			if (pointsMatrix[index][index2] == 0):
				if(test_adjacency(index,index2,2)):
					if(calc_possibility_score(index, index2) > bestScore):
						bestScore = calc_possibility_score(index, index2)
						possibilities.clear()
					if(calc_possibility_score(index, index2) == bestScore):
						possibilities.push_back([index,index2])
	return possibilities

var aiHitMaxY:bool = false
var aiHitMaxX:bool = false
var aiHitMinY:bool = false
var aiHitMinX:bool = false

func calc_possibility_score(posx:int, posy:int) -> int:
	var score:int = 0
	score += test_adjacency(posx, posy, 0)
	score += test_adjacency(posx, posy, 1)*3
	if(!aiHitMaxY): score += (11-posy)*3
	elif(!aiHitMinY) : score += (posy)
	if(!aiHitMaxX): score += (6-posx)*2
	elif(!aiHitMinX) : score += (posx)*2
	return score

func test_max_hits(posx:int, posy:int):
	if(posx==0):
		aiHitMaxX = true
	if(posx==11):
		aiHitMinX = true
	if(posy==0):
		aiHitMaxY = true
	if(posy==6):
		aiHitMinY = true


func pick_and_play(possibilities:Array):
	if(possibilities.size()):
		var action:Array = possibilities[randi()%possibilities.size()]
		play_as_ai(action[0], action[1])
	else:
		end_game(1)

func play_as_ai(posx:int, posy:int):
	pointsMatrix[posx][posy] = 2
	$Tiles.get_child(posx).get_child(posy).color = p2Color
	playingId = 1
	actualise()



func _on_BtnConcede_pressed():
	end_game(2)

func end_game(winner:int):
	$BtnConcede.visible = false
	$LblWinner.text = "Le joueur "+String(winner)+" a gagné !"



func actualise():
	$LblPlayer1.text = "Player 1 : " + String(count_score(1))
	$LblPlayer2.text = "Player 2 : " + String(count_score(2))

func count_score(player:int)->int:
	var total:int = 0
	for index in pointsMatrix.size():
		for index2 in pointsMatrix[0].size():
			if pointsMatrix[index][index2] == player:
				total += 1
	return total
